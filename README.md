# Photo-to-Drawing Style Transfer (MVI Semestral Work)

The goal of this project is to generate drawings/sketches from photos. This type of task is often called style transfer and popular approach for this task is to use GANs ([review](https://arxiv.org/abs/1705.04058)).

![checkpoint-results.png](checkpoint-results.png)

## Report

The semestral work is described in [report](report.pdf).

## Source code

Jupyter notebook with source code including short project description is available at [Kaggle](https://www.kaggle.com/davidmasek/public-photo-to-drawing-style-transfer). 

## Instalation/Running

The notebook is ready to run without setup. Outputs are included, so you don't have to.

For running outside of Kaggle see their [Docker image](https://github.com/Kaggle/docker-python). The main dependency is Tensorflow 2.6.0.

## Checkpoint

This section describes the "checkpoint" version. See above for the latest version.

Jupyter notebook with source code including project description is available at [Kaggle (checkpoint)](https://www.kaggle.com/davidmasek/mvi-checkpoint-photo-to-drawing-style-transfer/notebook). Trained model and sample results are also available at the same link on the [data](https://www.kaggle.com/davidmasek/mvi-checkpoint-photo-to-drawing-style-transfer/data) tab.

As a backup the same notebook is also available in this repository.